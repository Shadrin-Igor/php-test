<?php

class BaseModel {
    /**
     * Get all item in table
     */
    function getAll($options) {
        return App::DbService->types[$this->type]->tables[$this->table]->getAll($options);
    }

    /**
     * Get one item
     */
    function findOne($id) {
        return App::DbService->types[$this->type]->tables[$this->table]->getAll($options);
    }

    /**
     * Save one item
     */
    function save($data) {
        if($data->id) {
            return this.create($data);
        } else {
            return this.update($data);
        }
    }

    /**
     * Save one item
     */
    function create($data) {
        return App::DbService->types[$this->type]->tables[$this->table]->create($data);
    }

    /**
     * Save one item
     */
    function update($data) {
        return App::DbService->types[$this->type]->tables[$this->table]->update($data);
    }
}