<?php

class ExercisesController
{
    /**
     * Вытаскиваем список упраженний
     */
    function actionGetExercises()
    {
        $coutHTML = '';
        $list = App::models->exercise->getAll();
        for($i=0;$i<$list.length;$i++) {
            $coutHTML .= App::servies->ExerciseService->getView($list[$i]->type);
        }
        return $coutHTML;
    }

    /**
     * Получаем от пользователя ответ, расчитываем бал, сохраняем ответ, сохраняем историю, выдаем пользователю
     */
    function actionSaveExerciseAnswer()
    {
        $exerciseID = App::post->get('exerciseID');
        $exerciseAnswerData = App::post->get('exerciseAnswerData');
        $historyData = App::post->get('historyData');
        $countingAnswers = App::models->service->checkExercise->countingAnswers($exerciseID, $exerciseAnswerData);
        App::models->ExerciseAnsversModel->save($countingAnswers);
        App::models->ExerciseHistoryModel->save($historyData);
        $countingAnswers = App::models->service->ExerciseService->getResult($exerciseID);
        return $countingAnswers;
    }

    /**
     * Получаем от пользователя текущий статус прохождения и сохраняем в базу
     */
    function actionSaveExerciseStatus()
    {
        $statusData = App::post->get('statusData');
        App::models->ExerciseHistoryModel->save($statusData);

    }

    function actionSaveHistory()
    {
        // Получаем от пользователя текущий статус прохождения и сохраняем в базу
    }
}
