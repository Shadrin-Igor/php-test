<?php

class ExerciseService {
    function __construct()
    {
        $listType = [1=>'Type1ExerciseCounting', 2=>'Type2ExerciseCounting']; // Регистрируем какие типы расчетов у нас есть
        $viewType = [1=>'Select', 2=>'Input']; // Регистрируем какие типы отображени заданий у нас есть
    }

    public function countingAnswers($exerciseID, $exerciseAnswerData) {
        $exerciseData = App::models->ExerciseModel->get($exerciseID);
        $countingTypeClass = $this->listType[$exerciseData->countingType];
        $this->countingAnswers($countingTypeClass, $exerciseAnswerData);
    }

    private function countingType($countingTypeClass, $data) {
        $countingTypeClass->counting($data);
    }

    public getResult($idExercise) {
        return App::models->ExerciseHistoryModel->get($idExercise);
    }

    public getView($exerciseType) {
        return App::services->ViewService->getView($this->$viewType[$exerciseType]);
    }


}